let widgets = [{
    icon : "images/anydesk.png"
},{
    icon : "images/calender.png"
},{
    icon : "images/chrome.png"
},{
    icon : "images/finder.png"
},{
    icon : "images/mrd.png"
},{
    icon : "images/notes.png"
},{
    icon : "images/thunder.png"
},{
    icon : "images/vscode.png"
},{
    icon : "images/xd.png"
}]








// /* Start ups */

let dock = document.querySelector(".dock-container"),
modules = document.querySelectorAll(".icon-container")

AttachEventListener()




// if(modules.length > 0){
//     AttachEventListener()
// }else{
//     dock.style.display = "none"
// }



// // main

// function CreateModules(){
    
//     let modules = []

//     let imgs = [
//         "",
//         "./images/calender.png",
//         "./images/notes.png",
//         "./images/chrome.png",
//         "./images/mrd.png",
//         "./images/vscode.png",
//         "./images/xd.png",
//         "./images/thunder.png",
//         "./images/anydesk.png",
//     ]
//     for (let i = 0; i < imgs.length; i++) {
    
//         let div = document.createElement("div")
//         div.setAttribute("data-index", i)
//         div.classList.add("icon")
    
//         let img = document.createElement("img")
//         img.src = imgs[i]
//         div.appendChild(img)
//         dock.appendChild(div)
//         modules.push(div)
//     }
//     return modules
// }

function AttachEventListener(){
    
    // dock.addEventListener("mouseleave", evt => {
    //     for (let i = 0; i < modules.length; i++) {
    //         modules[i].style.height = modules[i].style.width = ""
    //     }
    // })

    for (let i = 0; i < modules.length; i++) {
        modules[i].addEventListener("mousemove", evt => {
            
            let target = getIconTarget(evt),
            x = getExactXpos(evt), 
            prevtarget = target.previousSibling,
            nexttarget = target.nextSibling

            // consider hover from 0
            let movepoint = 0.10638297872340426,
            prevElementHeight = 80,
            nexElementHeight = 70

            target.setAttribute("style",`height:${80}px;width:${80}px`)

            for (let i = 0; i < 3; i++) {
                
                point = (nexElementHeight - i*10) + (x * movepoint)
                if(isIconNode(nexttarget)){
                    nexttarget.style.height = nexttarget.style.width = `${point}px`
                    nexttarget = nexttarget.nextSibling
                }

                point = (prevElementHeight - i*10) - (x * movepoint)
                if(isIconNode(prevtarget)){
                    prevtarget.style.height = prevtarget.style.width = `${point}px`
                    prevtarget = prevtarget.previousSibling
                }

            }

        })
    }


}



// // helpers

function getIconTarget(evt){
    let target = null
    if(evt.target.classList.contains("icon-container"))
        target = evt.target
    else
        target = evt.target.closest(".icon-container")
    return target
}
function getExactXpos(e){
    let x = e.offsetX
    if(e.target.tagName == "IMG") x += 7
    else x+=1
    return x
}
function isIconNode(node){
    return node && node.classList && node.classList.contains("icon")
}


